package com.quwi.app.ui

import android.os.Bundle
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import com.quwi.app.R
import com.quwi.app.ui.common.BaseActivity


class MainActivity : BaseActivity() {
    override fun getLayoutResource(): Int = R.layout.activity_main
    override fun getNavigationResource(): Int =
        R.id.nav_host_fragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        if (navController.currentDestination?.id == R.id.memberAreaFragment) {
            finish()
            return false
        }
        return NavigationUI.navigateUp(navController, null)
    }

}