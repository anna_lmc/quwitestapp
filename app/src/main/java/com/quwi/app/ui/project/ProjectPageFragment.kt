package com.quwi.app.ui.project

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.quwi.app.R
import com.quwi.app.common.extensions.injectViewModel
import com.quwi.app.common.extensions.setRoundImage
import com.quwi.app.common.extensions.showSnack
import com.quwi.app.data.model.ProjectInfo
import com.quwi.app.ui.common.BaseFragment
import com.quwi.app.ui.member.area.MemberAreaViewModel
import com.quwi.app.ui.member.area.ProjectAdapter
import kotlinx.android.synthetic.main.fragment_member_area.*
import kotlinx.android.synthetic.main.fragment_project_page.*
import javax.inject.Inject

class ProjectPageFragment : BaseFragment() {
    override fun getFragmentLayout(): Int = R.layout.fragment_project_page

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: ProjectPageViewModel

    private lateinit var adapter: UsersAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = injectViewModel(viewModelFactory)
        viewModel.lvProjectInfo.observe(this.viewLifecycleOwner, observerProjectInfo())
        viewModel.lvMessage.observe(this.viewLifecycleOwner, observerMessage())
        viewModel.lvProgress.observe(this.viewLifecycleOwner, observerProgress())

        adapter = UsersAdapter()
        fr_project_page_rv_users.adapter = adapter
        fr_project_page_sr.setOnRefreshListener {
            getProjectInfo()
        }

        fr_project_page_rv_users.addItemDecoration(
            DividerItemDecoration(
                fr_project_page_rv_users.context,
                LinearLayoutManager.VERTICAL
            )
        )

        getProjectInfo()
    }

    private fun observerProjectInfo(): Observer<ProjectInfo> = Observer { info ->
        adapter.clear()

        info.users?.let {
            adapter.submitData(it)

        }

        fr_project_page_iv_icon.setRoundImage(info.logo_url)
        fr_project_page_tv_name.text = info.name
        fr_project_page_tv_time_week.text = getString(R.string.time_week, info.spent_time_week)
        fr_project_page_tv_time_month.text = getString(R.string.time_month, info.spent_time_month)
        fr_project_page_tv_time_all.text = getString(R.string.time_all, info.spent_time_month)
    }


    private fun observerMessage(): Observer<String> = Observer { message ->
        showSnack(message)
    }

    private fun observerProgress(): Observer<Boolean> = Observer { result ->
        fr_project_page_sr.isRefreshing = result
    }

    private fun getProjectInfo() {
        arguments?.let {
            ProjectPageFragmentArgs.fromBundle(it).apply {
                id?.let { id ->
                    viewModel.getProjectInfo(id)
                }
            }
        }
    }


}