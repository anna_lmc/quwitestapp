package com.quwi.app.ui.login


import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.quwi.app.R
import com.quwi.app.common.extensions.hideSoftKeyboard
import com.quwi.app.common.extensions.injectViewModel
import com.quwi.app.common.extensions.isValidEmail
import com.quwi.app.common.extensions.showSnack
import com.quwi.app.data.model.ServerError
import com.quwi.app.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

class LoginFragment : BaseFragment() {
    override fun getFragmentLayout(): Int = R.layout.fragment_login

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: LoginViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = injectViewModel(viewModelFactory)
        viewModel.lvAuthorization.observe(this.viewLifecycleOwner, observerAuthorization())
        viewModel.lvProgress.observe(this.viewLifecycleOwner, observerProgress())
        viewModel.lvMessage.observe(this.viewLifecycleOwner, observerMessage())
        fr_login_btn_login.setOnClickListener {
            val email = fr_login_et_email.text.toString()
            val password = fr_login_et_password.text.toString()
            if (isEmailValid(email) && isPasswordValid(password)) {
                viewModel.login(email, password)
                activity?.hideSoftKeyboard(fr_login_et_email)
            }
        }
        viewModel.addTextValidate(fr_login_et_email) {
            fr_login_ti_email.isErrorEnabled = !it
        }
        viewModel.addTextValidate(fr_login_et_password) {
            fr_login_ti_password.isErrorEnabled = !it
        }
        viewModel.isAuthorized()
    }

    private fun observerAuthorization(): Observer<Boolean> = Observer { result ->
        if (result) {
            findNavController().navigate(
                LoginFragmentDirections.actionLoginToMemberArea()
            )
        }
    }

    private fun observerProgress(): Observer<Boolean> = Observer { result ->
        fr_login_btn_login.isEnabled = !result
        fr_login_et_email.isEnabled = !result
        fr_login_et_password.isEnabled = !result
    }

    private fun observerMessage(): Observer<String> = Observer { message ->
        showSnack(message)
    }

    private fun isPasswordValid(password: String): Boolean {
        if (password.isEmpty()) {
            fr_login_ti_password.error = getString(R.string.field_empty)
            fr_login_ti_email.isErrorEnabled = true
            return false
        }
        return true
    }

    private fun isEmailValid(email: String): Boolean {
        if (email.isEmpty()) {
            fr_login_ti_email.error = getString(R.string.field_empty)
            fr_login_ti_email.isErrorEnabled = true
            return false
        }

        if (!email.isValidEmail()) {
            fr_login_ti_email.error = getString(R.string.invalid_email)
            fr_login_ti_email.isErrorEnabled = true
            return false
        }
        return true
    }

}