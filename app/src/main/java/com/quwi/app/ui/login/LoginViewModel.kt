package com.quwi.app.ui.login


import com.common.SingleLiveEvent
import com.quwi.app.common.NetManager
import com.quwi.app.data.sources.user.UserRepository
import com.quwi.app.data.sources.user.remote.requests.LoginParams
import com.quwi.app.ui.common.BaseViewModel

import javax.inject.Inject

class LoginViewModel @Inject constructor(
    netManager: NetManager,
    val repository: UserRepository
) : BaseViewModel(netManager) {

    private val _lvAuthorization = SingleLiveEvent<Boolean>()
    val lvAuthorization: SingleLiveEvent<Boolean>
        get() = _lvAuthorization


    fun isAuthorized() {
        _lvAuthorization.value = repository.isAuthorized()
    }

    fun login(email: String, password: String) {
        if (isInternetAvailable()) {
            addDisposable(compositeWrapper(repository.login(LoginParams(email, password))) {
                isAuthorized()
            })
        }
    }

}