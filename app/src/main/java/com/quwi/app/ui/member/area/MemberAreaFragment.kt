package com.quwi.app.ui.member.area

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.quwi.app.R
import com.quwi.app.common.extensions.injectViewModel
import com.quwi.app.common.extensions.showSnack
import com.quwi.app.data.model.Project
import com.quwi.app.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_member_area.*
import kotlinx.android.synthetic.main.fragment_project_page.*
import kotlinx.android.synthetic.main.layout_update_project.view.*
import javax.inject.Inject


class MemberAreaFragment : BaseFragment() {
    override fun getFragmentLayout(): Int = R.layout.fragment_member_area

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: MemberAreaViewModel

    private lateinit var adapter: ProjectAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = injectViewModel(viewModelFactory)
        viewModel.lvProjects.observe(this.viewLifecycleOwner, observerProject())
        viewModel.lvMessage.observe(this.viewLifecycleOwner, observerMessage())
        viewModel.lvProgress.observe(this.viewLifecycleOwner, observerProgress())
        viewModel.lvUpdatedProject.observe(this.viewLifecycleOwner, Observer { project ->
            viewModel.selectedProject?.let {
                adapter.updateItem(it, project)
            }
        })
        adapter = ProjectAdapter(
            {
                navigateToProjectPage(it.id)
            },
            { displayDialog(it) })
        fr_member_area_rv_projects.adapter = adapter
        viewModel.getProjects()

        fr_member_area_rv_projects.addItemDecoration(
            DividerItemDecoration(
                fr_member_area_rv_projects.context,
                LinearLayoutManager.VERTICAL
            )
        )

        fr_member_area_swipe_refresh.setOnRefreshListener {
            viewModel.getProjects()
        }
    }

    private fun observerProject(): Observer<List<Project>> = Observer { list ->
        adapter.submitData(list)
    }

    private fun displayDialog(item: Project) {
        viewModel.selectedProject = item.id
        val view = layoutInflater.inflate(R.layout.layout_update_project, null)
        view.lt_update_project_et_name.setText(item.name)
        val builderSingle = MaterialAlertDialogBuilder(context).apply {
            setTitle(R.string.change_ticket_subject)
            setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
                viewModel.updateProjects(view.lt_update_project_et_name.text.toString())
            }
            setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            setCancelable(true)
        }

        builderSingle.setView(view)
        builderSingle.show()
    }

    private fun navigateToProjectPage(id: String) {
        findNavController().navigate(
            MemberAreaFragmentDirections.actionMemberAreaToProjectPage(
                id = id
            )
        )
    }

    private fun observerMessage(): Observer<String> = Observer { message ->
        showSnack(message)
    }

    private fun observerProgress(): Observer<Boolean> = Observer { result ->
        fr_member_area_swipe_refresh.isRefreshing = result
    }
}