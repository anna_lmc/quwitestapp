package com.quwi.app.ui.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.quwi.app.di.annotation.Injectable

abstract class BaseFragment : Fragment(), Injectable {

    abstract fun getFragmentLayout(): Int


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getFragmentLayout(), container, false)
    }

    protected fun findNavController(): NavController = NavHostFragment.findNavController(this)

}