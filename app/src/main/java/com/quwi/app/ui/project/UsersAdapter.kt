package com.quwi.app.ui.project

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.quwi.app.R
import com.quwi.app.common.extensions.setRoundImage
import com.quwi.app.data.model.User

import kotlinx.android.synthetic.main.item_user.view.*

class UsersAdapter(
) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    private val items = ArrayList<User>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun submitData(newRating: List<User>) {
        val diffCallback = UserDiffCallback(items, newRating)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()
        items.addAll(newRating)
        diffResult.dispatchUpdatesTo(this)
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: User) {
            itemView.item_user_tv_name.text = item.name
            itemView.item_user_iv_avatar.setRoundImage(item.avatar_url, R.drawable.ic_baseline_person_24)
            itemView.item_user_tv_data.text = item.dta_activity
        }
    }
}