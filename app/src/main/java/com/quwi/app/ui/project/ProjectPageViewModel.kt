package com.quwi.app.ui.project

import com.common.SingleLiveEvent
import com.quwi.app.common.NetManager
import com.quwi.app.data.model.ProjectInfo
import com.quwi.app.data.sources.user.UserRepository
import com.quwi.app.ui.common.BaseViewModel
import javax.inject.Inject

class ProjectPageViewModel @Inject constructor(
    netManager: NetManager,
    val repository: UserRepository
) : BaseViewModel(netManager) {



    private val _lvProjectInfo = SingleLiveEvent<ProjectInfo>()
    val lvProjectInfo: SingleLiveEvent<ProjectInfo>
        get() = _lvProjectInfo

    fun getProjectInfo(id: String) {
        if (isInternetAvailable()) {
            addDisposable(compositeWrapper(repository.getProjectsInfo(id)) {
                _lvProjectInfo.value = it.project
            })
        }
    }

}