package com.quwi.app.common.extensions

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide

import com.bumptech.glide.request.RequestOptions
import com.quwi.app.R

fun ImageView.setRoundImage(url: String?, placeHolder : Int = R.drawable.ic_baseline_work_24) {
    Glide.with(context).load(url).apply(RequestOptions.circleCropTransform())
        .apply(RequestOptions.placeholderOf(placeHolder)).into(this)
}

