package com.quwi.app.common.extensions

import android.media.ExifInterface
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.util.Log
import android.util.Patterns

import java.io.IOException
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.*


fun String.isValidEmail(): Boolean {
    return this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
}


fun String.isMatch(input: String?): Boolean {
    if (isEmpty() || input.isNullOrEmpty())
        return false
    return this == input
}

fun String.encrypt(): String {
    return try {
        Base64.encodeToString(this.toByteArray(), Base64.NO_WRAP)
    } catch (e: Exception) {
        this
    }
}


fun String.decrypt(): String {
    return try {
        String(Base64.decode(this, Base64.NO_WRAP))
    } catch (e: Exception) {
        this
    }
}

fun String.toIntOrZero(): Int {
    return toIntOrNull() ?: 0
}


fun String.toLongOrZero(): Long {
    return toLongOrNull() ?: 0
}

fun String.toLongOrDefault(default: Long): Long {
    return toLongOrNull() ?: default
}

fun String.splitByComa(): Array<String> {
    return this.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
}

fun String.splitByHyphen(): Array<String> {
    return this.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
}

fun String.getEmails(): ArrayList<String> {
    val emails = ArrayList<String>()
    this.splitByComa().forEach { item ->
        if (item.trim { it <= ' ' }.isNotEmpty()) {
            emails.add(item.trim { it <= ' ' })
        }
    }

    return emails
}

fun String.getInitial(): String {
    val result = StringBuilder()
    val chars = this.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    for (item in chars) {
        if (item.isNotEmpty()) {
            result.append(item[0])
        }
        if (result.length == 2)
            break
    }
    return result.toString()
}

fun String.isValidPassword(): Boolean = this.length > 7

fun String.colorWord(from: Int, to: Int, color: Int): SpannableString {
    return SpannableString(this).apply {
        setSpan(ForegroundColorSpan(color), from, to, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
}

fun String.parseDate(pattern: String): Date? {
    val sdf: DateFormat = SimpleDateFormat(pattern, Locale.getDefault())
    return try {
        sdf.parse(this)
    } catch (e: ParseException) {
        null
    }
}

fun String.isValidDate(pattern: String): Boolean {
    val sdf: DateFormat = SimpleDateFormat(pattern, Locale.getDefault())
    sdf.isLenient = false
    try {
        sdf.parse(this)
    } catch (e: ParseException) {
        Log.d("isValidDate ", "isValidDate $this")
        Log.d("isValidDate ", "isValidDate " + e.message)
        return false
    }
    return true
}

infix fun String.default(value: String?): String? {
    return if (this.isEmpty()) {
        value
    } else {
        this
    }
}

fun String.getPhotoOrientation(): Int =
    try {
        ExifInterface(this).getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
    } catch (e: IOException) {
        ExifInterface.ORIENTATION_NORMAL
    }


fun String.fromHtml(): String =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY).toString()
    } else {
        Html.fromHtml(this).toString()
    }
