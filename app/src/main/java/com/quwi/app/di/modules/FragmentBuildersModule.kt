package com.quwi.app.di.modules
import com.quwi.app.ui.login.LoginFragment
import com.quwi.app.ui.member.area.MemberAreaFragment
import com.quwi.app.ui.project.ProjectPageFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberAreaFragment(): MemberAreaFragment


    @ContributesAndroidInjector
    abstract fun contributeProjectPageFragment(): ProjectPageFragment
}