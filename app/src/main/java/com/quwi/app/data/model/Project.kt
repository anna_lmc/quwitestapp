package com.quwi.app.data.model

data class Project(
    val dta_user_since: String,
    val id: String,
    val is_active: Int,
    val is_owner_watcher: Int,
    val logo_url: String?,
    val name: String,
    val position: Int,
    val uid: String,
    val users: List<User>

) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Project) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (uid != other.uid) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + uid.hashCode()
        return result
    }
}