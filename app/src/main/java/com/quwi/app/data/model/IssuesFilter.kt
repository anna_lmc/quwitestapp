package com.quwi.app.data.model

data class IssuesFilter(
    val is_open: Int
)