package com.quwi.app.data.sources.user.remote.response

import com.quwi.app.data.model.Project

data class UpdateProjectResponse (val project : Project)