package com.quwi.app.data.sources.user.remote.response

import com.google.gson.annotations.SerializedName
import com.quwi.app.data.model.AppInit

data class LoginResponse(
    @SerializedName("app_init")
    val appInit: AppInit?,
    @SerializedName("just_signup")
    val justSignUp: Boolean?,
    val token: String?
)