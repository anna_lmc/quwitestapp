package com.quwi.app.data.sources.user.local

import android.content.SharedPreferences
import com.quwi.app.common.string
import javax.inject.Inject

class SessionPreference  @Inject constructor(sharedPreferences: SharedPreferences) {
    var token: String? by sharedPreferences.string(TOKEN, null)
    private companion object Key {
        const val TOKEN = "TOKEN"
    }
}
