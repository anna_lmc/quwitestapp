package com.quwi.app.data.model

data class AppInit(
    val companies: List<Company>?,
    val has_own_company: Boolean?,
    val has_projects: Boolean?,
    val user: Profile?
)